# Training

This folder shall hold documents related to training in a group setting. If there are multiple documents related to a single training topic, please put them into a sub-directory.

## Ideas

Insert ideas for training sessions in this section

* Training for Group Participants
    * git
    * bash and other shells
    * "Before you learn a programming language, learn the (near) universal concepts."
    * collaboration using hosted git repositories
    * scripting
    * web frameworks, architectures, patterns (MVC vs MVT vs ELM vs ??)
    * APIs - what are they and how can they make tasks easier?
        * Basic read-only API endpoint authoring
        * Can probably also talk about using service webhooks during this session
    * automated testing (independent of languages)
    * Google Maps API in-depth - populating a map with custom location pins using Javascript

* Training for general SCA Populace (stuff that's mildly technical but would be of interest) - to be done as webinars and recorded/published publicly if at all possible
  * Using a PlayingCards.io game space for playing period games
    * how to use the game space editor
    * how to export your custom settings for reuse
    * how to import custom settings later
    * places you can find pre-made custom settings for period games
