# I Cifristi di Ea-nasir

The planning/project management repository for establishing a special-interest group focused on technologies that support <abbr title="Society for Creative Anachronism (a medievalist educational non-profit organization)">SCA</abbr> operations.

This is currently an unofficial project.

## About the Name
*cifrista* is a term mentioned in [The Professionalization of Cryptology in Sixteenth-Century Venice](https://www.cambridge.org/core/journals/enterprise-and-society/article/abs/professionalization-of-cryptology-in-sixteenthcentury-venice/4C1A7D44C76A4CD7F421A27D1CBDD4D5), which refers to *a person who is a professional cipher secretary* (that is, a person who handles *encoding from* and *decoding to* plain-text messages for their employer) in Sixteenth-Century Venice.

*cifrista* is not in use in Modern Italian.

On consultation with those familiar with Italian grammar, the plural of *cifrista* would be *cifristi*.

*Ea-nasir* was a Babylonian copper merchant we know about today because [someone wrote a complaint about the poor quality of his product](https://en.wikipedia.org/wiki/Complaint_tablet_to_Ea-nasir).

In the context of the purpose of this group, *Ea-nasir* is a reference to the copper that makes the Internet possible.

*I Cifristi della Ragnatela di Ea-Nasir* was also considered (to add a "spider's web" reference) but was decided to be too long for everyday use.

## About the Project

This is a collaborative project that welcomes those at any stage of experience. At minimum, you should be someone who regularly participates with your local branch. This project benefits from both *learners* and *teachers*, much like any *University* in the context of the organization would.

Preliminary Goals:
* Connect those who have tech experience with those who want to learn
* Provide a venue for collaboration
* Encourage sharing of institutional tech knowledge and ideas
* Increase the size of the pool of members available/willing to serve as branch Webminister
* Provide an environment where members are encouraged to use technology to solve existing problems - even if their solution doesn't pan out or isn't adopted
* Allow current webministers to round out their proficiencies

The agenda for each virtual session will be posted here. 

Suggestions for topics are welcome - please specify whether you want to learn about the topic or teach about the topic. The scope should be narrow enough to last no longer than 1 hour.

## Getting Started

Join the discussion:

* The first meeting is on Tuesday, July 5, 2022 at 8:30 PM (GMT-04:00) Eastern, US. [Request the Meeting Link](https://docs.google.com/forms/d/e/1FAIpQLScnH4HgY3gB1Ne3knAkJ9KBAjGLf7X9G9Dy8LplnZWPfUTpzg/viewform?usp=sf_link)
    * If there is enough interest from people for whom this time slot is inconvenient, either a different or additional time slot can be worked out.
* "Tech supporting SCA Operations" is also occasionally discussed in the "Service" channel of the Knowne World Discord server.

The first few meetings will include some introductory participatory instruction on the topics of establishing development environments, Linux-flavored terminal/shell familiarization, and basic `git` concepts. Until those happen, you can start with these resources:

* environments (doing this first will set you up for the following two top-level items in this list)
  * Platform-independent:
    * Use a Synology NAS to run Virtual Machines: [Virtual Machine Manager](https://www.synology.com/en-us/dsm/feature/virtual_machine_manager)
    * Canonical's Multipass: 
      * [on Windows](https://multipass.run/docs/installing-on-windows)
      * [on MacOS](https://multipass.run/docs/installing-on-macos)
      * [on Linux](https://multipass.run/docs/installing-on-linux)
  * On MacOS: [How to Set up an Apple Mac for Software Development](https://www.stuartellis.name/articles/mac-setup/)
  * On Windows: (note: WSL and Multipass do not play well together - you'll need to choose only one of the two)
    * Windows Subsystem for Linux: [Install Linux on Windows with WSL](https://docs.microsoft.com/en-us/windows/wsl/install)
  * On Linux:
    * Installing on an unused CPU: [Give an old PC New Life by Replacing Windows with Linux](https://www.ricksdailytips.com/replace-windows-with-linux/)
    * Setting up a dev environment on an unused Raspberry Pi: [Raspberry Pi 4 -- Yes, You Can Use it for Web Development](https://terracoders.com/blog/raspberry-pi-4-yes-you-can-use-it-web-development)
* shell basics
  * [The Linux Command Line for Beginners](https://ubuntu.com/tutorials/command-line-for-beginners)
  * [Learn Shell - Interactive Shell Tutorial](https://www.learnshell.org/)
  * [Learning the Shell](https://linuxcommand.org/lc3_learning_the_shell.php)
* git
  * [W3Schools git Tutorial](https://www.w3schools.com/git/)
  * [The Basics of git](https://codemag.com/Article/2201021/The-Basics-of-Git)
  * [git and Github for Beginners](https://www.freecodecamp.org/news/git-and-github-for-beginners/)
* programming concepts
  * [Human Resource Machine](https://tomorrowcorporation.com/humanresourcemachine) - A game that teaches programming concepts through puzzles (Steam, GOG, Humble, Windows, MacOS, Linux, Android, iOS, Nintendo Switch)
  * [Py](https://www.getpy.com/) - daily programming lessons (Android, iOS)
  * [Grasshopper](https://grasshopper.app/) - Javascript-centric lessons (Android)
  * [Intro to Computer Science on EdX](https://www.edx.org/course/introduction-computer-science-harvardx-cs50x) - free, self-paced, University-quality course
  * [MIT's Scratch](https://scratch.mit.edu/) - application designed to teach programming to children, but helpful for adults who are just starting
  * [Codecademy's Learn How to Code Module](https://www.codecademy.com/learn/learn-how-to-code)

## PRs Welcome

You are welcome to fork this repository and submit Pull Requests back to it. 

This repository may, at some point in the future, be transferred to a Group to allow management of multiple related resources, such as basic tutorials.

If you want to contribute but don't want to create a Gitlab account for the sole purpose of contributing to this project, I'll accept git patch files by email - send to (this repository's owner's Gitlab username, which consists of 6 numeric digits) @members.eastkingdom.org.
